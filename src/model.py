from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import StandardScaler
import numpy as np

class PipelineWrapper:
    def __init__(self):
        self.model = LinearRegression()
        self.scaler = StandardScaler()
    def fit(self,X,y):
        """

        :param X: pandas Dataframe or nunpy array with features, not including target variable
        :param y: np.array containing target variable Note that the shape is (n,2) as we have three targets!
        """
        pass


    def predict(self,X):
        """

        :param X: pandas Dataframe or numpy array with features, not including target variable
        :return: tuple of np.array with predictions for X (note that in fact you need to return three vectors)
        first element form a tuple represent causal and second registeed rents count
        """

        return (np.ones(X.shape[0]),np.ones(X.shape[0]))
